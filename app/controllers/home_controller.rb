class HomeController < ApplicationController
  include SpoonacularHelper
  include ActionController::Live
  #Home controller

  #def testing
  #  response.headers['Content-Type'] = 'text/event-stream'
  #  sse = SSE.new(response.stream, retry: 3000, event: "testing")
  #  sse.write({ value: 1 })
  #ensure
  #  response.stream.close
  #end


  def homepage
  end

  def about
  end

  def categories
  end
  
  def favouritespage
    @favourites = Favourite.all
  end

  def leaderboard
    @favourites = Favourite.all
  end

  def mealpage
    @recipe = getRecipeById(params[:recipe_id])
  end

  def show
  end

  def paleo
    @page_number = params[:page_number]
  end

  def vegan
    @page_number = params[:page_number]
  end

  def vegetarian
    @page_number = params[:page_number]
  end

  def pescatarian
    @page_number = params[:page_number]
  end

  
  


  
end
