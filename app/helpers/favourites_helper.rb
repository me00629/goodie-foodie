module FavouritesHelper
    def favourite_exists(user, recipe_id)
        result = false
        Favourite.all.each do |f|
          if f.user.email === user.email && f.recipe === recipe_id
            result = true
          end
        end

        return result
    end

    # returns a hash mapping recipe ID to number of times it has been favourited
    # sorted in descending order
    def get_leaderboard()
      favourites = {}
      Favourite.all.each do |favourite|
        if favourites[favourite.recipe] == nil
          favourites[favourite.recipe] = 1
        else
          favourites[favourite.recipe] += 1
        end
      end
      favourites_sorted = favourites.sort_by { |recipe, freq | -freq }
      return favourites_sorted
    end


end
